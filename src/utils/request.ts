import { request } from '@tarojs/taro';

type RequestOpts = Omit<request.Option, 'url'>;

const safeRequest = <T>(url: string, options: RequestOpts): Promise<T> => {
  return new Promise((resolve, reject) => {
    request({
      method: 'GET',
      ...options,
      header: {
        'Content-Type': 'application/json',
        ...options?.header,
      },
      url,
    }).then(
      response => {
        resolve(response?.data ?? response);
      },
      err => {
        reject(err);
      },
    );
  });
};

/**
 * get
 * @param url
 * @param opts
 * @returns {Promise}
 */
const get = async <T>(url: string, opts: RequestOpts): Promise<T> => {
  return safeRequest<T>(url, opts);
};

/**
 * post
 * @param url
 * @param opts
 * @returns {Promise}
 */
const post = async <T>(url: string, opts: RequestOpts): Promise<T> => {
  return safeRequest<T>(url, {
    ...opts,
    method: 'POST',
  });
};

/**
 * put
 * @param url
 * @param opts
 * @returns {Promise}
 */
const put = async <T>(url: string, opts: RequestOpts): Promise<T> => {
  return safeRequest<T>(url, {
    ...opts,
    method: 'PUT',
  });
};

export default {
  get,
  post,
  put,
};

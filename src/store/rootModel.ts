import { Models } from '@rematch/core';
import common from './models/common';

export interface RootModel extends Models<RootModel> {
  common: typeof common;
}

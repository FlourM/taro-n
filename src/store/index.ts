import { init, RematchRootState, RematchDispatch } from '@rematch/core';
import createLoadingPlugin from '@rematch/loading';
import { RootModel } from './rootModel';
import { models } from './loader';

const loadingPlugin = createLoadingPlugin({ number: true } as any);
interface ILoadingPlugin {
  loading: {
    models: RematchRootState<RootModel>;
    effects: Dispatch;
  };
}

export type RootState = RematchRootState<RootModel> & ILoadingPlugin;
export type Dispatch = RematchDispatch<RootModel>;

const configureStore = () => {
  const store = init({
    plugins: [loadingPlugin],
    models,
  });

  return store;
};

export default configureStore;

import { createModel } from '@rematch/core';
import { getLogin, LoginParams } from '../../services/demo';
import { RootModel } from '../rootModel';

interface CommonState {
  num: number;
}

export default createModel<RootModel>()({
  state: {
    num: 0,
  } as CommonState,
  reducers: {
    updateState(state, payload: ReducersPayloadAny) {
      return {
        ...state,
        ...payload,
      };
    },
  },
  effects: dispatch => ({
    async login(payload: LoginParams) {
      const res = await getLogin(payload);
      return res;
    },
    async increment(_payload, rootState) {
      dispatch.common.updateState({
        num: rootState.common.num + 1,
      });
    },
    async dec(_payload, rootState) {
      dispatch.common.updateState({
        num: rootState.common.num - 1,
      });
    },
  }),
});

export const currentEnv = process.env.FIG_ENV;
const ctext = currentEnv !== 'prod' ? `开课吧${currentEnv}` : '开课吧';

// 不要删除，用来识别当前项目环境
console.log(
  `\n %c ${ctext} %c https://xxx.com \n`,
  'color: #fff; background: #03a8e8; padding:5px 0; font-size:12px;font-weight: bold;',
  'background: #03a8e8; padding:5px 0; font-size:12px;',
);

export const isDevEnv = currentEnv === 'dev';
export const isPreEnv = currentEnv === 'pre';
export const isTestEnv = currentEnv === 'test';
export const isProdEnv = currentEnv === 'prod';

const baseApiUrl = {
  dev: 'https://opentest.xxx.com',
  test: 'https://opentest.xxx.com',
  pre: 'https://preopen2.xxx.com',
  prod: 'https://open2.xxx.com',
};

export const BASE_API_URL = baseApiUrl[currentEnv];

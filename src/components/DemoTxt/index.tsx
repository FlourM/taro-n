import React from 'react';
import { View, Text } from '@tarojs/components';
import styles from './index.module.scss';

const DemoTxt = props => {
  console.log('props', props);

  return (
    <View>
      <Text className={styles.textBody}>子组件内容{props.num}</Text>
    </View>
  );
};

export default DemoTxt;

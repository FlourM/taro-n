import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { View, Button } from '@tarojs/components';
import { navigateTo, useDidHide, useDidShow } from '@tarojs/taro';
import { RootState, Dispatch } from '../../store';
import DemoTxt from '../../components/DemoTxt';
import styles from './index.module.scss';

const Index = props => {
  const dispatch = useDispatch<Dispatch>();
  const commonStore = useSelector((state: RootState) => state.common);

  useEffect(() => {
    const init = async () => {
      const res = await dispatch.common?.login({
        contentType: 1,
        platType: 1,
      });

      console.log('请求数据', res);
    };
    init();
  }, [dispatch]);

  useDidShow(() => {
    console.log('componentDidShow');
  });

  useDidHide(() => {
    console.log('componentDidHide');
  });

  const goPage = () => {
    navigateTo({
      url: '/pages/home/index',
    });
  };
  return (
    <View className={styles.indexBody}>
      <Button className='add_btn' onClick={dispatch.common.increment}>
        +值
      </Button>
      <Button className='dec_btn' onClick={dispatch.common.dec}>
        -减
      </Button>
      <Button className='dec_btn' onClick={goPage}>
        跳转home页面
      </Button>
      <DemoTxt num={commonStore.num} />
    </View>
  );
};

export default Index;

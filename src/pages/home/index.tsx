import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { View, Button } from '@tarojs/components';
import { navigateTo } from '@tarojs/taro';
import DemoTxt from '../../components/DemoTxt';
import { RootState, Dispatch } from '../../store';
import styles from './index.module.scss';

const Home = props => {
  const dispatch = useDispatch<Dispatch>();
  const commonStore = useSelector((state: RootState) => state.common);

  const goPage = () => {
    navigateTo({
      url: '/pages/index/index',
    });
  };
  return (
    <View className={styles.indexBody}>
      <Button className='add_btn' onClick={dispatch.common.increment}>
        +{commonStore.num}
      </Button>
      <Button className='dec_btn' onClick={goPage}>
        回到首页
      </Button>
      <DemoTxt num={commonStore.num} />
    </View>
  );
};

export default Home;

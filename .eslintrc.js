module.exports = {
  extends: ['taro/react'],
  rules: {
    'react/jsx-uses-react': 'off',
    '@typescript-eslint/no-unused-vars': 0,
    'react/react-in-jsx-scope': 'off',
  },
};
